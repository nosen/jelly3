<?php
namespace Jelly;

use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $serviceManager = $e->getApplication()->getServiceManager();
        $eventManager        = $e->getApplication()->getEventManager();
        $sharedEventManager = $eventManager->getSharedManager();
        $routeListener = $serviceManager->get('Jelly\View\ViewFirstRouteListener');
        $injectVariableListener = $serviceManager->get('Jelly\View\InjectDefaultVariableListener');
        $processControlScriptListener = $serviceManager->get('Jelly\View\ProcessControlScriptListener');

        $eventManager->attach($routeListener);
        $injectVariableListener->attachShared($sharedEventManager);
        $processControlScriptListener->attachShared($sharedEventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/../../config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__
                ),
            ),
        );
    }
}
