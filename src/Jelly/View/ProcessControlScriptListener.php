<?php

namespace Jelly\View;

use Zend\EventManager\SharedEventManagerInterface;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

/**
 * execute controll script and inject return value into ViewModel
 * @author Naoki NOSE <nose@imjp.co.jp>
 */
class ProcessControlScriptListener extends AbstractDispatchListener
{
    const SUFFIX = '.ctrl.php';

    public function attachShared(SharedEventManagerInterface $events) {
        $this->listener = $events->attach('Zend\Stdlib\DispatchableInterface', MvcEvent::EVENT_DISPATCH, array($this, 'onDispatch'), -95);
    }

    public function detachShared(SharedEventManagerInterface $events) {
        $events->detach('Zend\Stdlib\DispatchableInterface', $this->listener);
    }

    public function onDispatch(MvcEvent $event)
    {
        $viewModel = $event->getResult();
        if (!$viewModel instanceof ViewModel) {
            return;
        }

        $template = $viewModel->getTemplate();
        if(!$template) {
            return;
        }

        $variables = $viewModel->getVariables();

        if ($variables instanceof \Traversable) {
            $variables = iterator_to_array($variables);
        }
        $variables = $this->processControlScript($template, $variables);
        $viewModel->setVariables($variables, true);
    }

    private function processControlScript($template, $variables)
    {
        /* @var $templatePathStack \Zend\View\Resolver\TemplatePathStack */
        $templatePathStack = $this->serviceLocator->get('ViewTemplatePathStack');

        foreach ($templatePathStack->getPaths() as $path) {
            $scriptFile = $path . $template . self::SUFFIX;
            if (file_exists($scriptFile) && is_readable($scriptFile)) {
                return $this->includeControlScript($scriptFile, $variables);
            }
        }
        return $variables;
    }

    private function includeControlScript($__name, $__vars)
    {
        extract($__vars);
        return array_merge($__vars, include($__name));
    }
}