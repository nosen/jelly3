<?php

namespace Jelly\View;

use Zend\Mvc\Controller\AbstractController;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

class ViewFirstController extends AbstractController
{
    public function onDispatch(MvcEvent $e)
    {
        $template = $e->getRouteMatch()->getParam('template');
        $config  = $this->getServiceLocator()->get('Config');
        $useLayout = isset($config['jelly']['use_layout']) ? $config['jelly']['use_layout'] : false;

        $model = new ViewModel();
        $model->setTemplate($template);
        if (!$useLayout) {
            $model->setTerminal(true);
        }
        $e->setResult($model);
        return $model;
    }
}
