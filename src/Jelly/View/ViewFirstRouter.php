<?php

namespace Jelly\View;

use Zend\Mvc\Router\Http\RouteInterface;
use Zend\Mvc\Router\Http\RouteMatch;
use Zend\Stdlib\RequestInterface as Request;

class ViewFirstRouter implements RouteInterface
{
    protected $routes = array();

    public static function factory($options = array()) {
        return new static($options);
    }

    public function __construct($options)
    {
        foreach ($options['routes'] as $route) {
            $this->routes[] = $this->analyzeRoute($route);
        }
    }

    private function analyzeRoute($route) {
        $template = null;
        $argNames = array();
        $paths = explode('/', $route);

        while (count($paths) > 0) {
            $last = $paths[count($paths) - 1];
            if (trim($last) === '') {
                array_pop($paths);
                continue;
            }
            if (strpos($last, ':') === 0) {
                array_unshift($argNames, substr($last, 1));
                array_pop($paths);
                continue;
            } else {
                $template = implode('/', $paths);
                break;
            }
        }
        return array(
            'template' => $template,
            'argNames' => $argNames
        );
    }

    public function match(Request $request, $pathOffset = null) {
        $params = array();

        $path = substr($request->getUri()->getPath(), $pathOffset);

        foreach ($this->routes as $route) {
            $match = $this->matchRoute($path, $route);
            if ($match) {
                return $match;
            }
        }
        return false;
    }

    private function matchRoute($path, $route)
    {
        $template = $route['template'];
        $argNames = $route['argNames'];

        if (strpos($path, $template) !== 0) {
            return false;
        }

        $paths = explode('/', substr($path, strlen($template)));

        $argValues = array();

        while(count($paths) > 0) {
            $first = array_shift($paths);
            if (trim($first) === '') {
                continue;
            }
            $argValues[] = $first;
        }
        $args = array_combine($argNames, $argValues);
        if ($args === false) {
            return false;
        }

        $params['controller'] = 'Jelly\View\ViewFirstController';
        $params['template'] = $template;
        $params['template_args'] = $args;

        return new RouteMatch($params);
    }

    public function assemble(array $params = array(), array $options = array()) {
        throw new \RuntimeException('Unsupported Operation');
    }

    public function getAssembledParams() {
        throw new \RuntimeException('Unsupported Operation');
    }

}
