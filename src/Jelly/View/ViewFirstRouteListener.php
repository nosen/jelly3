<?php

namespace Jelly\View;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Application;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mvc\Router\RouteMatch;

/**
 * Liseten MvcEvent::DISPACH_ERROR event.
 **/
class ViewFirstRouteListener implements ListenerAggregateInterface, ServiceLocatorAwareInterface
{
    /**
     * @var \Zend\Stdlib\CallbackHandler[]
     */
    protected $listeners = array();
    protected $serviceLocator;

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * Attach to an event manager
     *
     * @param  EventManagerInterface $events
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'onDispatchError'), 100);
    }

    /**
     * Detach all our listeners from the event manager
     *
     * @param  EventManagerInterface $events
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    public function onDispatchError($event)
    {
            $request = $event->getRequest();
            if ($request instanceof \Zend\Console\Request) {
                return;
            }
            $error = $event->getError();

            switch($error) {
                case Application::ERROR_ROUTER_NO_MATCH:

                    $path= $request->getUri()->getPath();
                    $basePath = $request->getBasePath();
                    $template = substr($path, strlen($basePath));

                    $lastChar = substr($template, strlen($template) - 1);
                    if ($lastChar === '/') {
                        $template = $template . 'index';
                    }

                    $renderer = $this->serviceLocator->get('ZfcTwigRenderer');

                    if(!$renderer->canRender($template)) {
                        return;
                    }

                    $event->setError(null);
                    $routeMatch = new RouteMatch(array(
                        'controller' => 'Jelly\View\ViewFirstController',
                        'template' => $template,
                        'inject_default_vars' => true,
                    ));
                    $event->setRouteMatch($routeMatch);

                    return $routeMatch;
                default:
                    return;

            }

    }

}
