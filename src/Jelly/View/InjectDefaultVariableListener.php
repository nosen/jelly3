<?php

namespace Jelly\View;

use Zend\EventManager\SharedEventManagerInterface;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

/**
 * injects default variable into ViewModel
 */
class InjectDefaultVariableListener extends AbstractDispatchListener
{
    private $listener;

    public function attachShared(SharedEventManagerInterface $events) {
        $this->listener = $events->attach('Zend\Stdlib\DispatchableInterface', MvcEvent::EVENT_DISPATCH, array($this, 'onDispatch'), -85);
    }

    public function detachShared(SharedEventManagerInterface $events) {
        $events->detach('Zend\Stdlib\DispatchableInterface', $this->listener);
    }

    public function onDispatch(MvcEvent $event)
    {
        $viewModel = $event->getResult();
        if (!$viewModel instanceof ViewModel) {
            return;
        }

        $enabled = $event->getRouteMatch()->getParam('inject_default_vars', false);
        if (!$enabled) {
            return;
        }

        $config = $this->serviceLocator->get('Config');
        $variables = $viewModel->getVariables();
        if (!isset($config['jelly']['default_variables'])) {
            return;
        }

        foreach ($config['jelly']['default_variables'] as $name => $value) {
            if (!isset($variables[$name])) {
                $variables[$name] = $this->getServiceLocator()->get($value);
            }
        }
       $viewModel->setVariables($variables, true);

    }
}
