<?php

return array(
    'service_manager' => array(
        'invokables' => array(
            'Jelly\View\ViewFirstRouteListener' => 'Jelly\View\ViewFirstRouteListener',
            'Jelly\View\InjectDefaultVariableListener' => 'Jelly\View\InjectDefaultVariableListener',
            'Jelly\View\ProcessControlScriptListener' => 'Jelly\View\ProcessControlScriptListener',
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Jelly\View\ViewFirstController' => 'Jelly\View\ViewFirstController'
        ),
    ),
    'jelly' => array(
        'default_variables' => array(
            'services' => 'ServiceManager',
            'request' => 'Request'
        )
    ),
);
