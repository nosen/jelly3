<?php

namespace Jelly\View;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;

class ViewFirstControllerTest extends \PHPUnit_Framework_TestCase
{
    private $controller;
    private $event;
    private $serviceManager;


    public function setUp()
    {
        $this->controller = new ViewFirstController;
        $this->event = new MvcEvent;
        $this->serviceManager = new \Zend\ServiceManager\ServiceManager;
        $this->controller->setServiceLocator($this->serviceManager);
    }

    public function testDispatch()
    {
        $routeMatch = new RouteMatch(array(
            'controller' => 'Jelly\View\ViewFirstController',
            'template' => '/hoge/fuga'
        ));
        $this->serviceManager->setService('Config', array());

        $this->event->setRouteMatch($routeMatch);
        $response = $this->controller->onDispatch($this->event);
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $response);
        $this->assertEquals('/hoge/fuga', $response->getTemplate());
        $this->assertSame($response, $this->event->getResult());

        $this->assertTrue($response->terminate());
    }


    public function testUseLayout()
    {
        $routeMatch = new RouteMatch(array(
            'controller' => 'Jelly\View\ViewFirstController',
            'template' => '/hoge/fuga'
        ));
        $this->serviceManager->setService('Config', array(
            'jelly' => array(
                'use_layout' => true,
            )
        ));

        $this->event->setRouteMatch($routeMatch);
        $response = $this->controller->onDispatch($this->event);
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $response);
        $this->assertEquals('/hoge/fuga', $response->getTemplate());
        $this->assertSame($response, $this->event->getResult());

        $this->assertFalse($response->terminate());
    }

}
