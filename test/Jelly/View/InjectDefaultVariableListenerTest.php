<?php

namespace Jelly\View;

use PHPUnit_Framework_TestCase;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceManager;
use Zend\Mvc\Router\RouteMatch;
use Zend\View\Model\ViewModel;

/**
 * Description of InjectDefaultVariableListenerTest
 *
 * @author Naoki NOSE <nose@imjp.co.jp>
 */
class InjectDefaultVariableListenerTest extends PHPUnit_Framework_TestCase
{

    private $listener;
    private $serviceManager;
    private $config;
    private $event;
    private $routeMatch;
    private $viewModel;

    public function setUp()
    {
        $this->listener = new InjectDefaultVariableListener;

        $this->config = array();
        $this->event = new MvcEvent();
        $this->routeMatch = new RouteMatch(array());
        $this->event->setRouteMatch($this->routeMatch);
        $this->viewModel = new ViewModel();

        $this->serviceManager = new ServiceManager();
        $this->listener->setServiceLocator($this->serviceManager);

    }

    public function testOnDispatch()
    {
        $this->routeMatch->setParam('inject_default_vars', true);
        $this->event->setResult($this->viewModel);

        $this->config['jelly'] = array(
            'default_variables' => array(
                's1' => 'Service1',
                's2' => 'Service2'
            )
        );

        $this->serviceManager
                ->setService('Config', $this->config)
                ->setService('Service1', 'Service1Impl')
                ->setService('Service2', 'Service2Impl');

        $this->listener->onDispatch($this->event);

        $this->assertEquals('Service1Impl', $this->viewModel->getVariable('s1'));
        $this->assertEquals('Service2Impl', $this->viewModel->getVariable('s2'));
    }

    public function testConfigNotSet()
    {
        $this->event->setResult($this->viewModel);

        $this->serviceManager
                ->setService('Config', array());

        $this->listener->onDispatch($this->event);

        $this->assertEquals(0, count($this->viewModel->getVariables()));
    }

    public function testResultNotSet()
    {
        //$this->event->setResult($this->viewModel);

        $this->listener->onDispatch($this->event);

        $this->assertEquals(0, count($this->viewModel->getVariables()));
    }

}
