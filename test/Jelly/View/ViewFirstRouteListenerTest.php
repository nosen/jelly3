<?php
namespace Jelly\View;

use Zend\Mvc\Application;
use Zend\Mvc\MvcEvent;
use Zend\Http\PhpEnvironment\Request;
use Zend\ServiceManager\ServiceManager;
use Zend\Uri\UriFactory;

class MockRenderer {
    private $canRender;
    public function __construct($canRender) {
        $this->canRender = $canRender;
    }

    function canRender() {
        return $this->canRender;
    }
}

class ViewFirstRouteListenerTest extends \PHPUnit_Framework_TestCase
{
    private $listener;
    private $event;
    private $request;
    private $serviceManager;
    private $config = array();

    public function setUp()
    {
        $this->request = new Request;
        $this->listener = new ViewFirstRouteListener;
        $this->event = new MvcEvent;
        $this->event->setRequest($this->request);
        $this->serviceManager = new ServiceManager();
        $this->serviceManager->setAllowOverride(true);
        $this->listener->setServiceLocator($this->serviceManager);
        $this->serviceManager->setService('Config', array('jelly' => $this->config));
    }


    public function templateFoundProvider() 
    {
        return array(
            array('/hoge/fuga', '/hoge/fuga'),
            array('/hoge/', '/hoge/index'),
            
        );
    }

    function testDummy() {
        
    }

    /**
     * @dataProvider templateFoundProvider
     */
    public function testTemplateFound($path, $template)
    {
        $this->request->setBasePath('/foo');
        $uri = UriFactory::factory('http://localhost/foo' . $path);
        $this->request->setUri($uri);
        $this->config['route_files'] = array();

        $this->serviceManager->setService('ZfcTwigRenderer', new MockRenderer(true));

        $this->event->setError(Application::ERROR_ROUTER_NO_MATCH);

        $routeMatch = $this->listener->onDispatchError($this->event);

        $this->assertSame($routeMatch, $this->event->getRouteMatch());
        $this->assertEquals('Jelly\View\ViewFirstController', $routeMatch->getParam('controller'));
        $this->assertEquals($template, $routeMatch->getParam('template'));
    }

    
}
