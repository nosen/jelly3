<?php

namespace Jelly\View;

use PHPUnit_Framework_TestCase;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Model\ViewModel;

/**
 *
 * @author Naoki NOSE <nose@imjp.co.jp>
 */
class ProcessControlScriptListenerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ProcessControlScriptListener
     */
    private $listener;
    private $serviceManager;
    private $templatePathStack;
    private $config;
    private $event;
    private $viewModel;

    public function setUp()
    {
        $this->listener = new ProcessControlScriptListener();

        $this->config = array();
        $this->event = new MvcEvent();
        $this->viewModel = new ViewModel();

        $this->templatePathStack = $this->getMock('\Zend\View\Resolver\TemplatePathStack', array('getPaths'));
        $paths = new \SplStack;
        $paths->push(__DIR__);
        $this->templatePathStack
             ->expects($this->any())
             ->method('getPaths')
             ->will($this->returnValue($paths));

        $this->serviceManager = new ServiceManager();
        $this->serviceManager
                ->setService('Config', $this->config)
                ->setService('ViewTemplatePathStack', $this->templatePathStack);
        $this->listener->setServiceLocator($this->serviceManager);

    }

    public function testOnDispatch()
    {
        $this->event->setResult($this->viewModel);
        $this->viewModel->setTemplate('/control_scripts/a');
        $var1 = new \stdClass();
        $var1->foo = 'bar';
        $this->viewModel->setVariable('var1', $var1);
        
        $this->listener->onDispatch($this->event);

        $this->assertEquals('b', $this->viewModel->getVariable('a'));
        $this->assertEquals('d', $this->viewModel->getVariable('c'));
        $this->assertEquals('bar', $this->viewModel->getVariable('foo'));
        $this->assertSame($var1, $this->viewModel->getVariable('var1'));
    }


    public function testScriptNotFound()
    {
        $this->event->setResult($this->viewModel);
        $this->viewModel->setTemplate('/no/such/script');
        $var1 = new \stdClass();
        $var1->foo = 'bar';
        $this->viewModel->setVariable('var1', $var1);

        $this->listener->onDispatch($this->event);

        $this->assertEquals(1, count($this->viewModel->getVariables()));
        $this->assertSame($var1, $this->viewModel->getVariable('var1'));
    }


    public function testVariableIsArray()
    {
        $this->event->setResult($this->viewModel);
        $this->viewModel->setTemplate('/control_scripts/a');
        $var1 = new \stdClass();
        $var1->foo = 'bar';
        $this->viewModel->setVariables(array('var1' => $var1), true);

        $this->listener->onDispatch($this->event);

        $this->assertEquals('b', $this->viewModel->getVariable('a'));
        $this->assertEquals('d', $this->viewModel->getVariable('c'));
        $this->assertEquals('bar', $this->viewModel->getVariable('foo'));
        $this->assertSame($var1, $this->viewModel->getVariable('var1'));
    }


    public function testResultIsNotSet()
    {
        //$this->event->setResult($this->viewModel);
        $this->viewModel->setTemplate('/control_scripts/a');
        $var1 = new \stdClass();
        $var1->foo = 'bar';
        $this->viewModel->setVariables(array('var1' => $var1), true);

        $this->listener->onDispatch($this->event);

        $this->assertEquals(1, count($this->viewModel->getVariables()));
        $this->assertSame($var1, $this->viewModel->getVariable('var1'));
    }


    public function testTemplateIsNotSet()
    {
        $this->event->setResult($this->viewModel);
        //$this->viewModel->setTemplate('/control_scripts/a');
        $var1 = new \stdClass();
        $var1->foo = 'bar';
        $this->viewModel->setVariables(array('var1' => $var1), true);

        $this->listener->onDispatch($this->event);

        $this->assertEquals(1, count($this->viewModel->getVariables()));
        $this->assertSame($var1, $this->viewModel->getVariable('var1'));
    }

}