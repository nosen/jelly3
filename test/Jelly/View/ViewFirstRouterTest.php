<?php

namespace Jelly\View;

use Zend\Http\PhpEnvironment\Request;
use Zend\Uri\UriFactory;

class ViewFirstRouterTest extends \PHPUnit_Framework_TestCase
{

    public function testMatch()
    {

        $request = new Request;

        $request->setUri(UriFactory::factory('http://localhost/a/b/c/foo/bar'));

        $router = ViewFirstRouter::factory(array(
            'routes' => array('/a/b/c/:d/:e')
        ));

        $match = $router->match($request, 0);
        
        $params = $match->getParams();
        $this->assertEquals('Jelly\View\ViewFirstController', $params['controller']);
        $this->assertEquals('/a/b/c', $params['template']);
        $this->assertEquals(array('d'=>'foo', 'e' =>'bar'), $params['template_args']);

    }
}
